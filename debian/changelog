python-selenium (4.24.4+dfsg-1) unstable; urgency=medium

  * [6a2ecd9] New upstream version 4.24.4+dfsg
  * [770fb47] Rebuild patch queue from patch-queue branch
    Updated patch:
    pytest-Ignore-trio_mode-for-now.patch
  * [f006af7] autopkgtest: Update setup due dropped file pytest.ini

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 01 Sep 2024 08:36:53 +0200

python-selenium (4.23.0+dfsg-1) unstable; urgency=medium

  * [5bd7a38] New upstream version 4.23.0+dfsg
  * [d1a5b28] Rebuild patch queue from patch-queue branch
    Added patch:
    setup.py-Ignore-rest_extension-requirement.patch
    Removed patches (included upstream):
    Add-missing-dependency-on-websocket-client.patch
    docs-Remove-non-exsting-modules.patch
  * [77be58e] d/control: Use pybuild-plugin-pyproject in B-D
  * [dacf7bc] d/control: Drop field Built-Using in p-selenium-doc

 -- Carsten Schoenert <c.schoenert@t-online.de>  Tue, 23 Jul 2024 14:56:09 +0900

python-selenium (4.22.0+dfsg-1) unstable; urgency=medium

  * [c8cb180] New upstream version 4.22.0+dfsg
  * [aa34e76] Rebuild patch queue from patch-queue branch
    Added patch:
    add-missing-dependency-on-websocket-client.patch
    Updated patch:
    docs-Remove-non-exsting-modules.patch
  * [78bbbef] d/control: Add new needed build dependency
  * [d0743c1] d/rules: Remove folder .mypy_cache in dh_clean call

 -- Carsten Schoenert <c.schoenert@t-online.de>  Mon, 08 Jul 2024 07:56:48 +0200

python-selenium (4.21.0+dfsg-1) unstable; urgency=medium

  * [e1134bd] d/gbp.conf: Filter out one more folder
  * [34f5348] d/copyright: Add excluded folder here too
  * [0c4d67c] New upstream version 4.21.0+dfsg
  * [fe1b65b] d/rules: Add build date data to -doc build

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 25 May 2024 11:03:42 +0200

python-selenium (4.20.0+dfsg-1) unstable; urgency=medium

  * [f0bde41] New upstream version 4.20.0+dfsg
  * [9bffeb9] d/control: Update Standards-Version to 4.7.0
    No further changes needed.
  * [4ea7606] d/control: Suggest python-selenium-doc in python3-selenium

 -- Carsten Schoenert <c.schoenert@t-online.de>  Wed, 01 May 2024 07:59:50 +0200

python-selenium (4.19.0+dfsg-1) unstable; urgency=medium

  * [c427f0f] New upstream version 4.19.0+dfsg
  * [521e2b7] Rebuild patch queue from patch-queue branch

 -- Carsten Schoenert <c.schoenert@t-online.de>  Mon, 01 Apr 2024 19:08:18 +0200

python-selenium (4.18.1+dfsg-1) unstable; urgency=medium

  * [1d3488f] New upstream version 4.18.1+dfsg

 -- Carsten Schoenert <c.schoenert@t-online.de>  Wed, 21 Feb 2024 17:32:53 +0100

python-selenium (4.17.2+dfsg-1) unstable; urgency=medium

  * [ae45cad] New upstream version 4.17.2+dfsg
  * [8165d19] Rebuild patch queue from patch-queue branch
    Added patch:
    docs-Remove-non-exsting-modules.patch
  * [1b981b3] d/control: Add python3-typing-extensions to B-D
  * [1824469] d/copyright: Update year data

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 27 Jan 2024 18:23:28 +0100

python-selenium (4.16.0+dfsg-1) unstable; urgency=medium

  * [ef4a886] New upstream version 4.16.0+dfsg
  * [8664b95] d/control: Use GitHub project site in Homepage

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 10 Dec 2023 06:29:51 +0100

python-selenium (4.15.2+dfsg-1) unstable; urgency=medium

  * [5687314] New upstream version 4.15.2+dfsg

 -- Carsten Schoenert <c.schoenert@t-online.de>  Thu, 23 Nov 2023 13:36:47 +0000

python-selenium (4.14.0+dfsg-1) unstable; urgency=medium

  * [61c1592] New upstream version 4.14.0+dfsg

 -- Carsten Schoenert <c.schoenert@t-online.de>  Fri, 20 Oct 2023 19:46:12 +0200

python-selenium (4.13.0-1) unstable; urgency=medium

  * [c01f83b] d/watch: Add compression type
  * [4919f67] New upstream version 4.13.0

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 01 Oct 2023 10:12:03 +0200

python-selenium (4.12.0+dfsg-2) unstable; urgency=medium

  * [f161db3] autopkgtest: Try to make test more robust
  * [e140e24] README.Debian: Move to d/python3-selenium.README.Debian
  * [48bc31a] python-selenium.NEWS: Move to d/python3-selenium.NEWS
  * [5b7fcdb] d/watch: Adjust watch logic again
  * [d19a93f] d/p-s.README.Debian: Correct small spelling issues

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 09 Sep 2023 15:12:49 +0530

python-selenium (4.12.0+dfsg-1) unstable; urgency=medium

  * [bd839ea] New upstream version 4.12.0+dfsg
  * [6cc97b7] Rebuild patch queue from patch-queue branch
    Removed patches (fixed upstream)
    ElementScrollBehavior-fix-for-ie-options.patch
    fixed-Enum-issue-in-ie-options.patch
  * [5b22b76] d/README.Debian: Add section about the Selenium Manager
  * [25b0d5f] d/NEWS moved to d/python-selenium.NEWS
    (Closes: #1050378)

 -- Carsten Schoenert <c.schoenert@t-online.de>  Fri, 01 Sep 2023 22:18:33 +0530

python-selenium (4.11.2+dfsg-1) unstable; urgency=medium

  * [b424e01] Revert "d/watch: Use @ANY_VERSION@ for package version"
  * [5cfa193] d/watch: Tune watch logic once more
  * [cfce316] New upstream version 4.11.2+dfsg
  * [a042fea] Rebuild patch queue from patch-queue branch
    Added patches:
    ElementScrollBehavior-fix-for-ie-options.patch
    fixed-Enum-issue-in-ie-options.patch
  * [d4e5968] d/copyright: Update information about documentation
  * [25eae52] autopkgtest: Set path to chromdriver binary

 -- Carsten Schoenert <c.schoenert@t-online.de>  Fri, 04 Aug 2023 20:56:46 +0200

python-selenium (4.10.0+dfsg-1) unstable; urgency=medium

  * [d9c4a0f] d/watch: Use @ANY_VERSION@ for package version
  * [40d562a] d/gbp.conf: Expand postunpack command
    (Closes: #1029236)
  * [3d43c55] New upstream version 4.10.0+dfsg
  * [a2a85bd] d/control: Add Built-Using to -doc package

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 17 Jun 2023 08:11:57 +0200

python-selenium (4.9.1+dfsg-1) unstable; urgency=medium

  * [2ffcff8] New upstream version 4.9.1+dfsg

 -- Carsten Schoenert <c.schoenert@t-online.de>  Mon, 29 May 2023 14:49:50 +0200

python-selenium (4.9.0+dfsg-1) unstable; urgency=medium

  * [02f43ba] New upstream version 4.9.0+dfsg
  * [e4e1dcd] Use a not so usual port for chromium test
    Use port 8088 instead of 8000, the port 8000 might be in use already by
    some other process.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 22 Apr 2023 13:02:57 +0200

python-selenium (4.8.3+dfsg-1) unstable; urgency=medium

  * [9118276] New upstream version 4.8.3+dfsg
  * [5bb3ae9] debian/: Move d/docs to d/python-selenium-doc.links

 -- Carsten Schoenert <c.schoenert@t-online.de>  Wed, 29 Mar 2023 12:14:56 +0200

python-selenium (4.8.2+dfsg-1) unstable; urgency=medium

  * [8e56110] New upstream version 4.8.2+dfsg

 -- Carsten Schoenert <c.schoenert@t-online.de>  Mon, 20 Feb 2023 20:12:51 +0100

python-selenium (4.8.1+dfsg-1) unstable; urgency=medium

  * [019f242] New upstream version 4.8.1+dfsg

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 18 Feb 2023 11:48:54 +0100

python-selenium (4.8.0+dfsg-1) unstable; urgency=medium

  * [3c1c4b1] New upstream version 4.8.0+dfsg
  * [2f824f7] d/control: Update Standards-Version to 4.6.2
    No further changes needed.
  * [f0b64a0] d/control: Move to dh-sequence*, add BuildProfileSpecs
  * [7067e82] d/rules: Remove --with option from default target
  * [eda9013] autopkgtest: Drop @builddeps@ and set python3-pytest
  * [5139a39] d/copyright: Update year data

 -- Carsten Schoenert <c.schoenert@t-online.de>  Wed, 25 Jan 2023 11:51:31 +0100

python-selenium (4.7.2+dfsg-1) unstable; urgency=medium

  * [7ed28d7] d/watch: Tune watch magic again
  * [b1d3c67] New upstream version 4.7.2+dfsg

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 04 Dec 2022 17:31:47 +0100

python-selenium (4.6.0+dfsg-1) unstable; urgency=medium

  * [1b4b674] d/gbp.conf: Filter out occurrences of 'rust' folders too
  * [38a744c] New upstream version 4.6.0+dfsg
  * [15f3303] Rebuild patch queue from patch-queue branch

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 06 Nov 2022 08:34:44 +0100

python-selenium (4.5.0+dfsg-1) unstable; urgency=medium

  * [450ad89] CI: Remove doubled entry lines
  * [16bea8d] d/watch: Look out also for versions without -python
  * [16c631a] New upstream version 4.5.0+dfsg
  * [b40c0a3] autopkgtest: Add --no-sandbox as we run headless

 -- Carsten Schoenert <c.schoenert@t-online.de>  Fri, 21 Oct 2022 20:26:18 +0200

python-selenium (4.4.2+dfsg-2) unstable; urgency=medium

  * Source only upload
  * [8808fd1] autopktest: Limit test-chromium to few architectures

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 09 Oct 2022 08:02:38 +0200

python-selenium (4.4.2+dfsg-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Use secure URI in Homepage field.
  * Bump debhelper from old 10 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update standards version to 4.6.0, no changes needed.

  [ Carsten Schoenert ]
  * [676a5d2] d/watch: Update file to version 4
    Rewrite the watch logic to monitor the git tags on the GitHub site.
    (Closes: #1019167)
  * [5beaac8] d/gbp.conf: Adjust config with [import-orig] section
    Use import-orig together with postunpack hook to repackage the upstream
    data before importing source data.
  * [aea0805] New upstream version 4.4.2+dfsg
  * [4c0f9f1] d/copyright: Update content and data
  * [e02b4d5] d/README.source: Update information about source
    Update the section about importing new source tarballs, also explain the
    usage of git-buildpackage as preferred tool for Debian packaging.
  * [93245a8] d/control: Update Standards-Version to 4.6.1
  * [45d111f] d/control: Running wrap-and-sort -ast
  * [53c22b3] d/control: Split off Build-Dep-Indep and add new deps
  * [3d5d0f1] python-selenium-doc: Add extra documentation package
  * [094a010] d/control: Update python3-selenium package description
  * [8ceba87] d/rules: Turn off verbose mode
  * [54f0faa] d/control: Remove non existing packages from Recommends
  * [72a813b] Rebuild patch queue from patch-queue branch
    Added patches:
    pytest-Ignore-trio_mode-for-now.patch
    tests-Ignore-WEBDRIVER-and-HTML_ROOT-variables.patch
    Renamed patch:
    0004-Readd-unmininmized-javascript-code.patch
    -> Readd-unmininmized-javascript-code.patch
  * [28440a2] d/rules: Add target override_dh_auto_test
  * [ed25dbe] d/control: Adding entry Rules-Requires-Root: no
  * [851b91a] d/u/metadata: Add some more project metadata
  * [18f98b6] autopkgtest: Add initial setup
  * [f3905af] d/py3dist-overrides: Adding override for trio_websocket
  * [e7049d5] d/copyright: Add copyright information about myself
  * [780da95] d/control: Add myself as uploader

  [ Johannes Schauer Marin Rodrigues ]
  * [2b92fc2] autopkgtest: Add Chrome webdriver testing
    (Closes: #923929)
  * [0d74bbc] d/salsa-ci.yml: Adding Yaml CI control data for Salsa

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 08 Oct 2022 09:07:58 +0200

python-selenium (4.0.0~a1+dfsg1-1) unstable; urgency=medium

  * Remove python2 dependencies (Closes: #938161)
  * New upstream version 4.0.0~a1+dfsg1
  * Remove dependencies and patches related to firefoxdriver (Closes: #93939)
  * Use cromium driver from path environment
  * Update deminimized js code from upstream
  * Remove unused lintian overrides

 -- Sascha Girrulat <sascha@girrulat.de>  Tue, 10 Sep 2019 11:25:42 +0200

python-selenium (3.14.1+dfsg1-1) unstable; urgency=medium

  * New upstream version 3.14.1+dfsg1
  * Bump Standarts-Version to 4.2.1
  * Add dependency to python[3]-urllib3
  * Update dependency to the related firefoxdriver

 -- Sascha Girrulat <sascha@girrulat.de>  Tue, 16 Oct 2018 16:13:17 +0200

python-selenium (3.8.0+dfsg1-3) unstable; urgency=medium

  * Shorten NEWS and move the content to README.Debian

 -- Sascha Girrulat <sascha@girrulat.de>  Sat, 03 Feb 2018 23:17:54 +0100

python-selenium (3.8.0+dfsg1-2) unstable; urgency=medium

  * Add newsfile to ease the handling with geckodriver.

 -- Sascha Girrulat <sascha@girrulat.de>  Tue, 12 Dec 2017 09:56:04 +0100

python-selenium (3.8.0+dfsg1-1) unstable; urgency=medium

  * New upstream version 3.8.0+dfsg1 (Closes: #883073)
  * Use https for debian watch
  * Update standards version
  * Rename old style config section
  * Update exludes to new package structure
  * Remove minimized javascript files from upstream code
  * Update patches to reflect upstream changes
  * Update changelog path to reflect upstream changes
  * Update package suggestions
  * Add patch file to readd unminimized javascript code

 -- Sascha Girrulat <sascha@girrulat.de>  Sat, 09 Dec 2017 23:15:28 +0100

python-selenium (2.53.2+dfsg1-2) unstable; urgency=medium

  * Add chromium-driver to recommends (Closes: #855353)
  * Update chromedriver binary location (Closes: #854732)

 -- Sascha Girrulat <sascha@girrulat.de>  Mon, 20 Feb 2017 11:05:02 +0100

python-selenium (2.53.2+dfsg1-1) unstable; urgency=medium

  * Update suggest of firefoxdriver to version 2.53.2

 -- Sascha Girrulat <sascha@girrulat.de>  Sun, 05 Jun 2016 22:03:13 +0200

python-selenium (2.53.2+dfsg1-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Added debian/gbp.conf.
  * New upstream release (Closes: #816624).
  * Ran wrap-and-sort -t -a.
  * Removed useless python build-depends (there's already python-all).
  * Removed useless X-Python-Version: and X-Python3-Version: fields, as the
    required versions are already available in oldoldstable.
  * Removed required version of python-setuptools (already satisfied in
    oldstable).
  * Bumped Standards-Version to 3.9.8 (no change).
  * Added extend-diff-ignore = "^[^/]*[.]egg-info/" in d/source/options.
  * Removed useless/harmful patch:
    - 0001-add-missing-files-to-SOURCES.txt.patch
    - 0003-Remove-superfluous-pbr-config.patch
    - 0004-changelog-naming.patch
  * Refreshed / rebased patches:
    - 0002-Set-xpi-path-to-usr-lib.patch
    - 0005-Use-x_ignore_nofocus-from-usr-lib-firefoxdriver.patch
  * Fixed README -> README.rst in debian/docs to follow what was done upstream.
  * Fixed debian/compat to 9 (which also didn't match debhelper version in
    Build-Depends).
  * Correctly install upstream changelog using dh_installchangelogs.
  * Do not attempt to package inexistant docs/* folder.
  * Now using HTTPS for VCS URLs.
  * Move chromedriver from Suggests to Recommends, and also makes phantomjs as
    an option, ie: chromedriver | phantomjs (Closes: #805732).
  * Add patch to fix chromedriver's location, thanks to Guido Günther for the
    patch (Closes: #805733).

 -- Thomas Goirand <zigo@debian.org>  Wed, 01 Jun 2016 10:16:42 +0200

python-selenium (2.48.0+dfsg1-2) unstable; urgency=medium

  * Lintian fixes:
    changelog should not mention-nmu
    capitalization error in description
    duplicate short description
    build depends on python-dev with no arch any

 -- Sascha Girrulat <sascha@girrulat.de>  Mon, 16 Nov 2015 21:45:27 +0100

python-selenium (2.48.0+dfsg1-1) unstable; urgency=medium

  * Imported Upstream version 2.48.0+dfsg1

 -- Sascha Girrulat <sascha@girrulat.de>  Thu, 05 Nov 2015 10:33:36 +0100

python-selenium (2.48.0-1) unstable; urgency=medium

  * Imported Upstream version 2.48.0
  * Use shared libs from /usr/lib/firefoxdriver

 -- Sascha Girrulat <sascha@girrulat.de>  Sun, 01 Nov 2015 00:33:18 +0100

python-selenium (2.47.1+dfsg1-2) unstable; urgency=medium

  * remove superfluous shlibs:Depends from depends
  * Add versioned suggest dependency to firefoxwebdriver
  * Update patches from gbp-pq

 -- Sascha Girrulat <sascha@girrulat.de>  Sat, 31 Oct 2015 21:19:01 +0100

python-selenium (2.47.1+dfsg1-1) experimental; urgency=medium

  [ Leo Arias ]
  * Added python3-setuptools as a dependency.

  [ Thomi Richards ]
  * Build python3 packages as well.

  [ Federico Gimenez ]
  * initial patch to move the package to main

  [Sascha Girrulat]
  * Import parts from a patch of Federico Gimenez <fgimenez@canonical.com>
  * Add get-orig-source target to debian/rules
    (Closes: #636677, #729269, #796319)
  * debian/control cleanups
  * Imported Upstream version 2.47.1+dfsg1
    (Closes: #705460, #659112, #760789 )
  * split off source patches from external patches
  * suggest chromdriver as one of the useable webdriver backends
    (Closes: #700061)
  * Remove x_ignore_nofocus.so

  [ Sascha Girrulat ]
  * Snapshot release 2.47.1+dfsg1-1~1.gbp824c36
  * switch source url from google svn to github
  * fix dversionmangle in debian/watch to compare the dfsg version
  * remove non-free from section (Closes: #770232)
  * use project homepage instead of pypi source page
  * add suggest to firefoxdriver
  * import parts of a patch from Federico Gimenez

 -- Sascha Girrulat <sascha@girrulat.de>  Mon, 24 Aug 2015 15:45:01 +0200

python-selenium (2.2.0-1) unstable; urgency=low

  * source package automatically created by stdeb 0.6.0+git
  * [120bd06] debianized (Closes: #626535)

 -- Sascha Girrulat <sascha@girrulat.de>  Sat, 30 Jul 2011 20:42:23 +0200
